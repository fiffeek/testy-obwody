#!/bin/bash

#
# Author: Witalis Domitrz <witekdomitrz@gmail.com>
#

# Information strings
################################################################################
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color

SUDO='sudo'

VALGRIND='valgrind --error-exitcode=15 --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --log-file='

DESC=$'Bash script testing given program with tests from given directory.
All failed tests` filenames are saved in err.log file.'
USAGE='Usage:
\t<script> <executable file name> <directory name>

Terminology:
\t<script name> \t- this script
\t<executable file name> \t- name of the tested executable
\t<directory name> \t- name of the directory containing tests

Optional test files format:
\t<test name>.in \t- input
\t<test name>.args \t- command line arguments
\t<test name>.out \t- expected output (stdout)
\t<test name>.err \t- expected error output (stderr)
\t<test name>.code \t- expected return code'
################################################################################


# Show description and exit
################################################################################
echo_error() {
	printf "${RED}Error${NC}\n"
}

echo_ok() {
	printf "${GREEN}OK${NC}\n"
}

echo_no_test() {
	printf "${YELLOW}No test${NC}\n"
}

echo_results() {
	printf "Total: $1\n"
	printf "${GREEN}Passed${NC} (No errors): $2\n"
	printf "${RED}Failed${NC} (At least one error): $3\n"
}

exit_with_error() {
	printf "${RED}Error:${NC} $1\n\n\n"
	printf "$USAGE\n"
	exit 1
}

exit_with_description() {
	printf "$DESC\n\n\n"
	printf "$USAGE\n"
	exit
}
################################################################################


# Check numer of arguments and verify if file and directory exists
################################################################################
if [ $# == 0 ]; then
	exit_with_description
fi
if [ $# != 2 ]; then
	exit_with_error "Wrong number of arguments. Expected 2 arguments."
fi
if [ ! -f $1 ]; then
	exit_with_error "Executable file is needed."
fi
if [ ! -d $2 ]; then
	exit_with_error "Directory with tests is needed."
fi
################################################################################


# Prepare files
################################################################################
execfile=`realpath $1`
directory=`realpath $2`
outfile=`mktemp`
errfile=`mktemp`
outcode=`mktemp`
default_argsfile=`mktemp`
default_infile=`mktemp`
errlogs="err.log"
touch $outfile
touch $errfile
touch $outcode
touch $default_argsfile
touch $default_infile
touch $errlogs
################################################################################


# Default arguments, input and log files preparation
################################################################################
echo "" > $default_argsfile
echo "" > $default_infile
echo "" > $errlogs
################################################################################


# Resetting counters
################################################################################
tests_no=0
passed_no=0
failed_no=0
################################################################################


# Start validation
################################################################################
for test in $directory/*.in ; do

	# Choosing test
	########################################################################
	test=${test%.*}
	printf "Running test: \n"
	printf "\tName: $test\n"
	########################################################################

	# Checking existence of arguments file
	########################################################################
	if [ -f $test.args ] ; then
		argsfile=$test.args
	else
		argsfile=$default_argsfile
	fi
	########################################################################
	
	# Checking existence of input file
	########################################################################
	if [ -f $test.in ] ; then
		infile=$test.in
	else
		infile=$default_infile
	fi
	########################################################################

	# Setting valgrind on/off 
	########################################################################
	if true ; then
		mkdir -p "valgrind_logs"
		valgrind_command=$VALGRIND
		test_name=${test%/*}
		test_name=${test#${test_name}}
		valgrind_command+="valgrind_logs/${test_name}.log"
		echo $valgrind_command
	else
		valgrind_command=""
	fi
	########################################################################

	# Setting sudo on/off
	########################################################################
	if false ; then
		sudo_command=$SUDO
	else
		sudo_command=""
	fi
	########################################################################

	# Running test
	########################################################################
	$sudo_command $valgrind_command $execfile `cat $argsfile` < $infile 1> $outfile 2> $errfile
	echo $? > $outcode
	########################################################################
	
	# Preparing flags
	########################################################################
	failed=false
	checked=false
	########################################################################
	
	# Validating results (stdout)
	########################################################################
	printf "\tstdout: \t"
	if [ ! -f $test.out ] ; then
		echo_no_test
	else
		checked=true
		if ! ( diff $outfile $test.out > /dev/null ) ; then
			echo_error
			failed=true
		else
			echo_ok
		fi
	fi
	########################################################################
	
	# Validating results (stderr)
	########################################################################
	printf "\tstderr: \t"
	if [ ! -f $test.err ] ; then
		echo_no_test
	else
		checked=true
		if ! ( diff $errfile $test.err > /dev/null ) ; then
			echo_error
			failed=true
		else
			echo_ok
		fi
	fi
	########################################################################
	
	# Validating results (outcode)
	########################################################################
	printf "\toutcode: \t"
	if [ ! -f $test.code ] ; then
		echo_no_test
	else
		checked=true
		if ! ( diff $outcode $test.code > /dev/null ) ; then
			echo_error
			failed=true
		else
			echo_ok
		fi
	fi
	########################################################################
	
	# Increasing appropriate counters
	########################################################################
	if $checked ; then
		(( tests_no++ ))
		# Checking if the test has passed
		if $failed ; then
			(( failed_no++ ))
			echo $test >> $errlogs
		else
			(( passed_no++ ))
		fi
	fi
	########################################################################
	
	# Finishing test
	########################################################################
	printf "\n"
	########################################################################
done
echo_results $tests_no $passed_no $failed_no
exit 0
################################################################################
